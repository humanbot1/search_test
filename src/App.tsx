import React from 'react';
import './App.css';
import Container from 'react-bootstrap/Container';
import { SideMenu } from './components/SideMenu';
import { Search } from './components/Search';
import { Members } from './components/Members';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();
const App: React.FC = () => {
  return (
    <Router>
      <SideMenu />
      <main id="page-wrap">
        <Container className="pt-5">
          <Switch>
            <Route exact path="/" component={Search} />
            <Route path="/members/:memberpolicy" component={Members} />
          </Switch>
        </Container>
      </main>
    </Router>
  );
};

export default App;
