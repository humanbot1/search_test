export type State = {
  members: response[];
};

export type response = {
  id: number;
  firstName: string;
  lastName: string;
  memberCardNumber: string;
  policyNumber: string;
  dataOfBirth: string;
};

export type MemberContextData = {
  member: response[];
};

// type errorType = {
//   serviceDate: string;
//   policyNumber: string;
//   memberCardNumber?: string | undefined;
// };
