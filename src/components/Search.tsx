import React, { useState, useContext } from 'react';
import { Container, Form, Row, Col, Button } from 'react-bootstrap';
import 'react-datepicker/dist/react-datepicker.css';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import { useHistory } from 'react-router-dom';
import { GlobalContext } from '../context/GlobalProvider';

const api_url = `https://rcvp3-api.azurewebsites.net/members`;

type searchUserType = {
  serviceDate: Date | null | undefined;
  policyNumber: string;
  memberCardNumber?: string | undefined;
};

export const Search: React.FC = () => {
  const history = useHistory();
  const { dispatch } = useContext(GlobalContext);
  // console.log(`HISTORY: ${JSON.stringify(history)}`);
  const [btnValue, setBtnValue] = useState(false);

  const [searchUser, setSearchUser] = useState<searchUserType>({
    serviceDate: undefined,
    policyNumber: '',
    memberCardNumber: '',
  });

  const [errors, setErrors] = useState({
    serviceDate: '',
    policyNumber: '',
    memberCardNumber: '',
  });

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    validateForm(searchUser);

    const { serviceDate, policyNumber, memberCardNumber } = searchUser;
    console.log(`data: ${serviceDate}, ${policyNumber}, ${memberCardNumber}`);
    localStorage.setItem('policynumber', policyNumber);

    axios.get(api_url).then((results) => {
      dispatch({ type: 'GET_MEMBER', payload: results.data });
      handleReset();
      if (results.status === 200) {
        history.push(`/members/${policyNumber}`);
        // history.push({
        //   pathname: '/members',
        //   search: `?policyNumber=${policyNumber}`,
        // });
      }
    });
  };

  const validateForm = (fieldName: string | any) => {
    const errors = {
      serviceDate: '',
      policyNumber: '',
      memberCardNumber: '',
    };

    if (!fieldName.policyNumber) {
      errors.policyNumber = 'Policy number is required';
      setErrors(errors);
    } else if (isNaN(fieldName.policyNumber)) {
      errors.policyNumber = 'Not a number';
      setErrors(errors);
    } else if (fieldName.policyNumber.length < 10) {
      errors.policyNumber = 'Policy Number should be 10 digit';
      setErrors(errors);
    }
    if (!isNaN(fieldName.policyNumber) && fieldName.policyNumber.length === 10) {
      errors.policyNumber = '';
      setErrors(errors);
    }

    return errors;
  };

  const handleChange = (fieldName: string | any) => (e: React.ChangeEvent<HTMLInputElement>) => {
    // let target = e.target as HTMLInputElement;
    const value = e.currentTarget.value;
    // const name = e.target.name;
    console.log({ [fieldName]: value });
    setSearchUser({ ...searchUser, [fieldName]: value });
    setBtnValue(true);
  };

  const handleReset = () => {
    setSearchUser({
      serviceDate: undefined,
      policyNumber: '',
      memberCardNumber: '',
    });
  };

  return (
    <Container>
      <Form onSubmit={handleSubmit}>
        <Row>
          <Col>
            <Form.Label>Service Date</Form.Label>
          </Col>
        </Row>
        <Row>
          <Col>
            <DatePicker
              // dateFormat="yyyy/MM/dd"
              // selected={searchUser.serviceDate}
              onChange={(e) => handleChange('serviceDate')}
            />
          </Col>
        </Row>

        <Form.Label>Serach Using</Form.Label>
        <Form.Control
          // required
          type="text"
          placeholder="12345678"
          value={searchUser.policyNumber}
          onChange={handleChange('policyNumber')}
        />
        <Form.Text style={{ color: 'red' }}> {errors.policyNumber}</Form.Text>

        <Form.Label>Member Card Number</Form.Label>
        <Form.Control type="text" value={searchUser.memberCardNumber} onChange={handleChange('memberCardNumber')} />

        <Row className="mt-5">
          <Col>
            <Button variant="primary" type="submit" disabled={!btnValue}>
              Submit
            </Button>
          </Col>
          <Col>
            <Button variant="primary" type="Reset" onClick={handleReset}>
              Reset
            </Button>
          </Col>
        </Row>
      </Form>
    </Container>
  );
};
