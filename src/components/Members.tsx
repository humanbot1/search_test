import React, { useContext } from 'react';
import { GlobalContext } from '../context/GlobalProvider';
import { response } from '../types/Types';
import { Card, Row, Col } from 'react-bootstrap';
import { useLocation, useParams } from 'react-router-dom';

type memberProps = {
  member: response[];
  memberpolicy: string;
};

const Member = (props: memberProps) => {
  const { member, memberpolicy } = props;

  const param = new URLSearchParams(useLocation().search);
  const policyNum = param.get('policyNumber');
  console.log(`Policy string number: ${JSON.stringify(policyNum)}`);

  const memberData = member.find((p: response) => p.policyNumber === memberpolicy);

  return (
    <>
      {memberData ? (
        <Card className="text-center" key={memberData.id}>
          <Card.Body>
            <Row>
              <Col>First Name</Col>
              <Col>{memberData.firstName}</Col>
            </Row>
            <Row>
              <Col>Last Name</Col>
              <Col>{memberData.lastName}</Col>
            </Row>
            <Row>
              <Col>Date of Birth</Col>
              <Col>{memberData.dataOfBirth}</Col>
            </Row>
          </Card.Body>
        </Card>
      ) : (
        <span>Member not found!</span>
      )}
    </>
  );
};

export const Members: React.FC = () => {
  const { state } = useContext(GlobalContext);
  const { members } = state;
  const { memberpolicy } = useParams<{ memberpolicy: string }>();
  return (
    <div>
      <h3>Member</h3>
      <Member member={members} memberpolicy={memberpolicy} />
    </div>
  );
};
