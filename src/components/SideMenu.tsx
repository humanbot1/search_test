import React, { useState } from 'react';
// import NavLinkPath from './Link';
import { Link } from 'react-router-dom';
import { slide as Menu } from 'react-burger-menu';

export const SideMenu: React.FC = () => {
  const memberpolicy = localStorage.getItem('policynumber');

  const [menuOpen, setMenuOpen] = useState(false);

  const handleStateChange = (state: any) => {
    setMenuOpen(state.isOpen);
  };
  const closeMenu = () => {
    setMenuOpen(false);
  };

  return (
    <Menu isOpen={menuOpen} onStateChange={(state: any) => handleStateChange(state)}>
      <Link onClick={() => closeMenu()} className="menu-item" to="/">
        Home
      </Link>
      <Link onClick={() => closeMenu()} className="menu-item" to={`/members/${memberpolicy}`}>
        Search Result
      </Link>
    </Menu>
  );
};
