import React from 'react';
import { Link } from 'react-router-dom';
import Nav from 'react-bootstrap/Nav';

// https://github.com/react-bootstrap/react-bootstrap/issues/3944

type linkprops = {
  to: string;
  children: React.ReactNode;
  // onClick: () => void;
  className: string;
};

// eslint-disable-next-line no-undef
const NavLinkPath = (props: linkprops): JSX.Element => {
  return (
    <Nav.Link
      as={Link}
      to={props.to}
      // onClick={props.onClick}
      className={props.className}
    >
      {props.children}
    </Nav.Link>
  );
};

export default NavLinkPath;
