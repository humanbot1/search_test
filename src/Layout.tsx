/* eslint-disable no-use-before-define */
import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import { SideMenu } from './components/SideMenu';
import { Search } from './components/Search';
import { Members } from './components/Members';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

export const Layout: React.FC = () => {
  return (
    <Router history={history}>
      <SideMenu />
      <main>
        <Container>
          <Switch>
            <Route path={`/`} component={Search} />
            <Route path={`/Members`} component={Members} />
            {/* <Route path="/Member" component={Member} /> */}
            {/* <Route path={`/Member${param}`} component={Member} /> */}
          </Switch>
        </Container>
      </main>
    </Router>
  );
};
