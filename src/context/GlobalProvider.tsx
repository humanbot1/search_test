import React, { createContext, useReducer, PropsWithChildren, Dispatch } from 'react';
// import { State, response } from "../types/Types";

type Context = { state: State; dispatch: Dispatch<Action> };

export type State = {
  members: response[];
  isLoading: boolean;
};

export type response = {
  id: number;
  firstName: string;
  lastName: string;
  memberCardNumber: string;
  policyNumber: string;
  dataOfBirth: string;
};

type Props = {
  children?: React.ReactNode;
};

type Action = GET_MEMBER;

type GET_MEMBER = {
  type: 'GET_MEMBER';
  payload: response[];
};

const initialState: Context = {
  state: {
    members: [],
    isLoading: true,
  },
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  dispatch: (_a) => {},
};

export const AppReducer = (state: State, action: Action): State => {
  switch (action.type) {
    case 'GET_MEMBER':
      console.log(`DISPATCH CALLED: ${JSON.stringify(action.payload)}`);
      return {
        members: [...state.members, ...action.payload],
        isLoading: false,
      };

    default:
      return state;
  }
};

export const GlobalContext = createContext(initialState);

export const GlobalProvider = ({ children }: PropsWithChildren<Props>) => {
  const [state, dispatch] = useReducer(AppReducer, initialState.state);
  return <GlobalContext.Provider value={{ state, dispatch }}>{children}</GlobalContext.Provider>;
};
